import java.util.Scanner;

public class Game {
	Board board ;
	private Player X;
	private Player O ;
	Game() {
		X = new Player('X');
		O = new Player('O');
		board = new Board(X,O);
	}
	
	public void play() {
		
		showWelcome();
		while (true) {
			showBoard();
			showTurn();
			inputPosition();
			if(board.isFinish()) {
				break;
			}
			board.switchTurn();
		}
		showBoard();
		showResult();
		showStat();
		board.clearBoard();
	}
	
	public void showWelcome() {
		System.out.println(board.getWelcome());
	}

	public void showBoard() {
		board.showBoard();
	}

	public void inputPosition() {
		Scanner kb = new Scanner(System.in);
		while (true) {
			System.out.print("Plz choose position (R,C) :");
			String R = kb.next();
			String C = kb.next();
			if (board.setPosition(R, C)) {
				break;
			}
		}
	}

	public void showTurn() {
		System.out.println("Turn: " + board.getCurrent().getName());
	}
	public void showResult() {
		if(board.getWinner() != null) {
			if(board.getWinner().getName() == 'X') {
				X.addWin();
				O.addLose();
			}else {
				O.addWin();
				X.addLose();
			}
			System.out.println(board.getWinner().getName() + " : Win");
		}else {
			X.addDraw();
			O.addDraw();
			System.out.println("Draw");
		}
	}

	public void showStat() {
		System.out.println("Stat of Player");
		System.out.println("Player:" + X.getName() + " [Win:" + X.getWin() + " Lose: " + X.getLose() + " Draw: "
				+ X.getDraw()+ "]");
		System.out.println("Player:" + O.getName() + " [Win:" + O.getWin() + " Lose: " + O.getLose() + " Draw: "
				+ O.getDraw()+ "]");
		System.out.println();
	}
	
}
