
public class Player {
	private char name ;
	private int win ;
	private int lose ;
	private int draw ;
	
	
	Player(char name){
		this.name = name ;
		this.win = 0 ;
		this.lose = 0 ;
		this.draw = 0 ;
	}
	
	public void addWin(){
		win++ ;
	}
	public void addLose(){
		lose++ ;
	}
	public void addDraw(){
		draw++ ;
	}
	public int getWin(){
		return win ;
	}
	public int getLose() {
		return lose;
	}
	public int getDraw() {
		return draw ;
	}
	public char getName() {
		return name ;
	}
	public void setName(char name) {
		this.name = name ;
	}
}
